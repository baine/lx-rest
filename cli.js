
let yargs = require('yargs')
let LXRest = require('./index')

function main() {

  let responseLogger = function (err, response) {
    if (err) {
      console.log(err)
    } else {
      console.log(JSON.stringify(response))
    }
  }

  let rest = new LXRest()
  yargs
    .command(
      'post-order',
      'post an order',
      {
        order_type: {
          choices: ['limit', 'market'],
          default: 'limit'
        },
        contract_id: {
          type: 'number',
          demandOption: true
        },
        is_ask: {
          type: 'boolean',
          demandOption: true
        },
        swap_purpose: {
          choices: ['bf_hedge', 'non_bf_hedge', 'undisclosed'],
          default: 'undisclosed'
        },
        size: {
          type: 'number',
          default: 1
        },
        confirmed_reasonable: {
          type: 'boolean',
          default: false
        },
        price: {
          type: 'number',
          demandOption: true
        }
      },
      function (argv) {
        let {
          order_type,
          contract_id,
          is_ask,
          price,
          swap_purpose,
          size,
          confirmed_reasonable
        } = argv
        rest.postOrder({
          order_type,
          contract_id,
          is_ask,
          swap_purpose,
          size,
          confirmed_reasonable,
          price
        }, responseLogger)
      })
    .command(
      'cancel-all',
      'cancel all orders',
      {},
      () => rest.cancelAll(responseLogger))
    .command(
      'cancel-order',
      'cancel an order',
      function (yargs) {
        return yargs
          .option('mid', {
            demandOption: true
          })
          .option('contract_id', {
            type: 'number',
            demandOption: true
          })
      },
      function (argv) {
        let { mid, contract_id } = argv
        rest.cancelOrder(mid, { contract_id }, responseLogger)
      })
    .command(
      'edit-order',
      'edit an order',
      {
        mid: {
          demandOption: true
        },
        contract_id: {
          type: 'number',
          demandOption: true
        },
        is_ask: {
          type: 'boolean',
          demandOption: true
        },
        size: {
          type: 'number',
          default: 1
        },
        price: {
          type: 'number',
          demandOption: true
        }
      },
      function (argv) {
        let { mid, contract_id, is_ask, price, size} = argv
        rest.editOrder(mid, {
          contract_id, is_ask, price, size
        }, responseLogger)
      })
    .command(
      'orders',
      'get orders',
      {},
      () => rest.getOrders(responseLogger))
    .command(
      'balances',
      'get balances',
      {},
      () => rest.getCollateralBalances(responseLogger))
    .command(
      'positions',
      'get positions',
      {
        limit: {
          type: 'number',
          default: 100
        },
        offset: {
          type: 'number',
          default: 0
        }
      },
      function (argv) {
        let { limit, offset } = argv
        rest.getPositions({ limit, offset }, responseLogger)
      })
    .command('all-positions',
             'get all positions',
             () => rest.getAllPositions(responseLogger))
    .command(
      'all-contracts',
      'get all contracts',
      () => rest.getAllContracts(responseLogger))
    .command(
      'contracts',
      'get contracts',
      {
        limit: {
          type: 'number',
          default: 100
        },
        offset: {
          type: 'number',
          default: 0
        }
      },
      function (argv) {
        let { limit, offset } = argv
        rest.getContracts({limit, offset}, responseLogger)
      })
    .command(
      'trades',
      'get trades',
      {
        limit: {
          type: 'number',
          default: 100
        },
        offset: {
          type: 'number',
          default: 0
        }
      },
      function (argv) {
        let {limit, offset} = argv
        rest.getTrades({limit, offset}, responseLogger)
      })
    .command(
      'all-trades',
      'get all trades',
      () => rest.getAllTrades(responseLogger))
    .command(
      'my-trades',
      'get my trades',
      {
        limit: {
          type: 'number',
          default: 100
        },
        offset: {
          type: 'number',
          default: 0
        }
      },
      function(argv) {
        let {limit, offset} = argv;
        rest.getMyTrades({limit, offset}, responseLogger);
      })
    .command(
      'all-my-trades',
      'get all my trades',
      () => rest.getAllMyTrades(responseLogger))
    .command(
      'transactions',
      'get transaction history',
      {
        asset_type: {
          choices: ['BTC', 'USD'],
          demandOption: true
        },
        limit: {
          type: 'number',
          default: 100
        },
        offset: {
          type: 'number',
          default: 0
        }
      },
      function (argv) {
        let { limit, offset, asset_type } = argv
        rest.getTransactionHistory({limit, offset, asset_type}, responseLogger)
      })
    .command(
      'book',
      'get book',
      {
        contract_id: {
          type: 'number',
          demandOption: true
        }
      },
      function (argv) {
        let { contract_id } = argv
        rest.getBook(contract_id, responseLogger)
      })
    .command(
      'greeks',
      'get greeks',
      {},
      () => rest.getGreeks(responseLogger))
    .demandCommand()
    .help()
    .argv
}

if (require.main === module) {
  main()
}


let request = require('request')

function getAll (getter, params, data, cb) {
  params = params || {
    limit: 100,
    offset: 0
  }
  params.limit = params.limit || 100
  params.offset = params.offset || 0

  data = data || []
  const timeout = 100
  getter(params, function (err, result) {
    if (err) {
      return cb(err)
    }
    data = data.concat(result.data)
    if (result.meta.next) {
      params.offset = params.offset + params.limit
      setTimeout(getAll, timeout, getter, params, data, cb)
    } else {
      cb(null, { data })
    }
  })
}

function issueRequest (req, retryStrategy, attemptCount, method, endpoint, token, path, params, cb) {
  Object
    .keys(params)
    .forEach(function (k) {
      if (params[k] == null) {
        delete params[k]
      }
    })
  req({
    method: method,
    qs: params,
    uri: `${endpoint}${path}`,
    json: true,
    headers: {
      Authorization: `JWT ${token}`
    }
  }, function (err, httpResponse, body) {
    if (err) {
      cb(err, body)
    } else {
      let retry = retryStrategy(httpResponse, attemptCount)
      if (retry.retry) {
        console.log(`backing off for ${retry.backoff} ms`)
        setTimeout(issueRequest, retry.backoff, req, retryStrategy, attemptCount + 1, method, endpoint, token, path, params, cb)
      } else {
        cb(err, body)
      }
    }
  })
}

function LXRest (options = {}) {
  let {endpoint, token, r, retryStrategy} = options
  endpoint = endpoint || 'https://trade.ledgerx.com/api'
  token = token || process.env.LEDGERX_API_KEY
  r = r || request
  retryStrategy = retryStrategy || function (response, attemptCount) {
    if (attemptCount < 5 && response && response.statusCode) {
      // retry 5xx and 429
      if (response.statusCode >= 500 || response.statusCode === 429) {
        return {
          retry: true,
          backoff: (0.5 + Math.random()) * Math.pow(2, attemptCount + 1) * 1000
        }
      }
    }
    return {
      retry: false,
      backoff: null
    }
  }

  this.request = (method, path, params, cb) => issueRequest(
    r,
    retryStrategy,
    0,
    method,
    endpoint,
    token,
    path,
    params,
    cb
  )

  this.postOrder = ({
    order_type,
    contract_id,
    is_ask,
    swap_purpose,
    size,
    confirmed_reasonable,
    price
  }, cb) => issueRequest(
    r,
    retryStrategy,
    0,
    'POST',
    endpoint,
    token,
    '/orders',
    {
      order_type,
      contract_id,
      is_ask,
      swap_purpose,
      size,
      confirmed_reasonable,
      price
    },
    cb)

  this.cancelAll = (cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'DELETE',
      endpoint,
      token,
      '/orders',
      {},
      cb)

  this.cancelOrder = (mid, { contract_id }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'DELETE',
      endpoint,
      token,
      `/orders/${mid}`,
      { contract_id },
      cb)

  this.editOrder = (mid, {contract_id, is_ask, price, size}, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'POST',
      endpoint,
      token,
      `/orders/${mid}/edit`,
      { contract_id, is_ask, price, size },
      cb)

  this.getOrders = cb =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/open-orders',
      {},
      cb)

  this.getCollateralBalances = cb =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/balance',
      {},
      cb)

  this.getPositions = ({ limit, offset }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/positions',
      { limit, offset },
      cb)

  this.getAllPositions = (cb) => getAll(this.getPositions, null, null, cb)

  this.getContracts = ({ limit, offset }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/contracts',
      { limit, offset },
      cb)

  this.getAllContracts = (cb) => getAll(this.getContracts, null, null, cb)

  this.getTrades = ({ limit, offset }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/trades/global',
      { limit, offset },
      cb)

  this.getAllTrades = (cb) => getAll(this.getTrades, null, null, cb)

  this.getMyTrades = ({ limit, offset }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/trades',
      { limit, offset },
      cb)

  this.getAllMyTrades = (cb) => getAll(this.getMyTrades, null, null, cb)

  this.getTransactionHistory = ({ limit, offset, asset_type }, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/balance/history',
      { limit, offset, asset_type },
      cb)

  this.getAllTransactionHistory = ({ asset_type }, cb) =>
    getAll(this.getTrades, { asset_type }, null, cb)

  this.getBook = (contract_id, cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      `/book-states/${contract_id}`,
      {},
      cb)
  this.getGreeks = (cb) =>
    issueRequest(
      r,
      retryStrategy,
      0,
      'GET',
      endpoint,
      token,
      '/greeks',
      {},
      cb)
}

module.exports = LXRest

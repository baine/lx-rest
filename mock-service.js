
let deepEqual = require('deep-equal');

const mockToken = 'MOCK-TOKEN';

function mockR(items) {
    return function(request, cb) {
        // console.log(request);
        for (let x of items) {
            if (deepEqual(request, x.request)) {
                return cb(x.err, null, x.response);
            }
        }
        return cb(new Error('mock r function doesnt know what to do'));
    };
}

const mockService = [
    {
        request: {
            headers: {
                Authorization: `JWT ${mockToken}`
            },
            method: 'GET',
            qs: {
            },
            uri: 'https://trade.ledgerx.com/api/state',
            json: true
        },
        response: { data: { open_orders: [] } }
    },
    {
        request: {
            headers: {
                Authorization: `JWT ${mockToken}`
            },
            method: 'GET',
            qs: {
            },
            uri: 'https://trade.ledgerx.com/api/contracts',
            json: true
        },
        response: {
            data: [{
                id: 11790667,
                name: null,
                label: 'BTC 2018-12-28 Call $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790699,
                name: null,
                label: 'BTC 2018-12-28 Call $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11989423,
                name: null,
                label: 'BTC 2018-12-28 Call $50000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 5000000,
                min_increment: 25,
                date_live: '2017-12-09 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790668,
                name: null,
                label: 'BTC 2018-12-28 Put $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790697,
                name: null,
                label: 'BTC 2018-12-28 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790698,
                name: null,
                label: 'BTC 2018-12-28 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109459,
                name: null,
                label: 'BTC 2018-12-28 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790700,
                name: null,
                label: 'BTC 2018-12-28 Put $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109460,
                name: null,
                label: 'BTC 2018-12-28 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11989424,
                name: null,
                label: 'BTC 2018-12-28 Put $50000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 5000000,
                min_increment: 25,
                date_live: '2017-12-09 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790635,
                name: null,
                label: 'BTC 2018-09-28 Call $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109457,
                name: null,
                label: 'BTC 2018-09-28 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109458,
                name: null,
                label: 'BTC 2018-09-28 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            },{
                id: 11790666,
                name: null,
                label: 'BTC 2018-09-28 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790665,
                name: null,
                label: 'BTC 2018-09-28 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790636,
                name: null,
                label: 'BTC 2018-09-28 Put $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 14598042,
                name: null,
                label: 'BTC 2018-06-29 Put $20000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 2000000,
                min_increment: 25,
                date_live: '2018-02-12 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 14598041,
                name: null,
                label: 'BTC 2018-06-29 Call $20000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 2000000,
                min_increment: 25,
                date_live: '2018-02-12 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109427,
                name: null,
                label: 'BTC 2018-06-29 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790634,
                name: null,
                label: 'BTC 2018-06-29 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12672213,
                name: null,
                label: 'BTC 2018-06-29 Call $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-26 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790633,
                name: null,
                label: 'BTC 2018-06-29 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12672214,
                name: null,
                label: 'BTC 2018-06-29 Put $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-26 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790603,
                name: null,
                label: 'BTC 2018-06-29 Call $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790604,
                name: null,
                label: 'BTC 2018-06-29 Put $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109428,
                name: null,
                label: 'BTC 2018-06-29 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087711,
                name: null,
                label: 'BTC 2018-04-27 Call $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087748,
                name: null,
                label: 'BTC 2018-04-27 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087743,
                name: null,
                label: 'BTC 2018-04-27 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087747,
                name: null,
                label: 'BTC 2018-04-27 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087713,
                name: null,
                label: 'BTC 2018-04-27 Call $7500.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 750000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087714,
                name: null,
                label: 'BTC 2018-04-27 Put $7500.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 750000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087745,
                name: null,
                label: 'BTC 2018-04-27 Call $12500.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1250000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087746,
                name: null,
                label: 'BTC 2018-04-27 Put $12500.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1250000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087712,
                name: null,
                label: 'BTC 2018-04-27 Put $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 15087744,
                name: null,
                label: 'BTC 2018-04-27 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2018-02-24 05:00:00+0000',
                date_expires: '2018-04-27 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109398,
                name: null,
                label: 'BTC 2018-03-30 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-03-30 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 14687226,
                name: null,
                label: 'BTC 2018-03-30 Put $7500.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 750000,
                min_increment: 25,
                date_live: '2018-02-14 05:00:00+0000',
                date_expires: '2018-03-30 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12672184,
                name: null,
                label: 'BTC 2018-03-30 Put $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-26 05:00:00+0000',
                date_expires: '2018-03-30 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109397,
                name: null,
                label: 'BTC 2018-03-30 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-03-30 20:00:00+0000',
                derivative_type: 'options_contract' } ],
            meta: {
                limit: 40,
                offset: 0,
                next: '/contracts?offset=40&limit=40',
                previous: null,
                total_count: 275
            }
        }
    },
    {
        request: {
            headers: {
                Authorization: `JWT ${mockToken}`
            },
            method: 'GET',
            qs: {
                offset: 120
            },
            uri: 'https://trade.ledgerx.com/api/contracts',
            json: true
        },
        response: {
            data: [ {
                id: 12966830,
                name: null,
                label: 'BTC 2018-01-30 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-29 22:30:00+0000',
                date_expires: '2018-01-30 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12926678,
                name: null,
                label: 'BTC 2018-01-29 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-28 22:30:00+0000',
                date_expires: '2018-01-29 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12805886,
                name: null,
                label: 'BTC 2018-01-28 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-27 22:30:00+0000',
                date_expires: '2018-01-28 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12805885,
                name: null,
                label: 'BTC 2018-01-27 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-26 22:30:00+0000',
                date_expires: '2018-01-27 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12389883,
                name: null,
                label: 'BTC 2018-01-26 Call $20000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                type: 'call',
                strike_price: 2000000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12389881,
                name: null,
                label: 'BTC 2018-01-26 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12389913,
                name: null,
                label: 'BTC 2018-01-26 Call $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                type: 'call',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 13508575,
                name: null,
                label: 'BTC 2018-01-26 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2018-01-16 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 13508576,
                name: null,
                label: 'BTC 2018-01-26 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: false,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2018-01-16 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12389914,
                name: null,
                label: 'BTC 2018-01-26 Put $25000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: false,
                type: 'put',
                strike_price: 2500000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12389882,
                name: null,
                label: 'BTC 2018-01-26 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: false,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12389884,
                name: null,
                label: 'BTC 2018-01-26 Put $20000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: false,
                type: 'put',
                strike_price: 2000000,
                min_increment: 25,
                date_live: '2017-12-19 05:00:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 12805884,
                name: null,
                label: 'BTC 2018-01-26 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-25 22:30:00+0000',
                date_expires: '2018-01-26 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12765594,
                name: null,
                label: 'BTC 2018-01-25 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-24 22:30:00+0000',
                date_expires: '2018-01-25 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12725442,
                name: null,
                label: 'BTC 2018-01-24 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-23 22:30:00+0000',
                date_expires: '2018-01-24 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12685150,
                name: null,
                label: 'BTC 2018-01-23 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-22 22:30:00+0000',
                date_expires: '2018-01-23 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12644854,
                name: null,
                label: 'BTC 2018-01-22 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-21 22:30:00+0000',
                date_expires: '2018-01-22 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12523978,
                name: null,
                label: 'BTC 2018-01-21 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-20 22:30:00+0000',
                date_expires: '2018-01-21 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12523977,
                name: null,
                label: 'BTC 2018-01-20 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-19 22:30:00+0000',
                date_expires: '2018-01-20 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12523976,
                name: null,
                label: 'BTC 2018-01-19 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-18 22:30:00+0000',
                date_expires: '2018-01-19 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12483686,
                name: null,
                label: 'BTC 2018-01-18 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-17 22:30:00+0000',
                date_expires: '2018-01-18 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12443394,
                name: null,
                label: 'BTC 2018-01-17 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-16 22:30:00+0000',
                date_expires: '2018-01-17 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12403102,
                name: null,
                label: 'BTC 2018-01-16 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-15 22:30:00+0000',
                date_expires: '2018-01-16 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12362804,
                name: null,
                label: 'BTC 2018-01-15 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-14 22:30:00+0000',
                date_expires: '2018-01-15 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12241984,
                name: null,
                label: 'BTC 2018-01-14 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-13 22:30:00+0000',
                date_expires: '2018-01-14 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12241983,
                name: null,
                label: 'BTC 2018-01-13 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-12 22:30:00+0000',
                date_expires: '2018-01-13 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12241982,
                name: null,
                label: 'BTC 2018-01-12 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-11 22:30:00+0000',
                date_expires: '2018-01-12 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12201776,
                name: null,
                label: 'BTC 2018-01-11 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-10 22:30:00+0000',
                date_expires: '2018-01-11 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12161540,
                name: null,
                label: 'BTC 2018-01-10 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-09 22:30:00+0000',
                date_expires: '2018-01-10 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12121248,
                name: null,
                label: 'BTC 2018-01-09 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-08 22:30:00+0000',
                date_expires: '2018-01-09 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 12080956,
                name: null,
                label: 'BTC 2018-01-08 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-07 22:30:00+0000',
                date_expires: '2018-01-08 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11960190,
                name: null,
                label: 'BTC 2018-01-07 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-06 22:30:00+0000',
                date_expires: '2018-01-07 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11960189,
                name: null,
                label: 'BTC 2018-01-06 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-05 22:30:00+0000',
                date_expires: '2018-01-06 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11960188,
                name: null,
                label: 'BTC 2018-01-05 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-04 22:30:00+0000',
                date_expires: '2018-01-05 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11919898,
                name: null,
                label: 'BTC 2018-01-04 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-03 22:30:00+0000',
                date_expires: '2018-01-04 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11882122,
                name: null,
                label: 'BTC 2018-01-03 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-02 22:30:00+0000',
                date_expires: '2018-01-03 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11841886,
                name: null,
                label: 'BTC 2018-01-02 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2018-01-01 22:30:00+0000',
                date_expires: '2018-01-02 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11801676,
                name: null,
                label: 'BTC 2018-01-01 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2017-12-31 22:30:00+0000',
                date_expires: '2018-01-01 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11681758,
                name: null,
                label: 'BTC 2017-12-31 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2017-12-30 22:30:00+0000',
                date_expires: '2017-12-31 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            }, {
                id: 11681757,
                name: null,
                label: 'BTC 2017-12-30 Swap',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: false,
                min_increment: 25,
                date_live: '2017-12-29 22:30:00+0000',
                date_expires: '2017-12-30 21:00:00+0000',
                derivative_type: 'day_ahead_swap'
            } ],
            meta: {
                limit: 40,
                offset: 120,
                next: '/contracts?offset=160&limit=40',
                previous: '/contracts?offset=80&limit=40',
                total_count: 275
            }
        }
    }, {
        request: {
            headers: {
                Authorization: `JWT ${mockToken}`
            },
            method: 'GET',
            qs: {
                limit: 5
            },
            uri: 'https://trade.ledgerx.com/api/contracts',
            json: true
        },
        response: {
            data: [ {
                id: 11790667,
                name: null,
                label: 'BTC 2018-12-28 Call $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11989424,
                name: null,
                label: 'BTC 2018-12-28 Put $50000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 5000000,
                min_increment: 25,
                date_live: '2017-12-09 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11989423,
                name: null,
                label: 'BTC 2018-12-28 Call $50000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 5000000,
                min_increment: 25,
                date_live: '2017-12-09 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790697,
                name: null,
                label: 'BTC 2018-12-28 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790668,
                name: null,
                label: 'BTC 2018-12-28 Put $5000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-12-28 21:00:00+0000',
                derivative_type: 'options_contract'
            } ],
            meta: {
                limit: 5,
                offset: 0,
                next: '/contracts?offset=5&limit=5',
                previous: null,
                total_count: 275
            }
        }
    }, {
        request: {
            headers: {
                Authorization: `JWT ${mockToken}`
            },
            method: 'GET',
            qs: {
                limit: 5,
                offset: 15
            },
            uri: 'https://trade.ledgerx.com/api/contracts',
            json: true
        },
        response: {
            data: [ {
                id: 11790665,
                name: null,
                label: 'BTC 2018-09-28 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-09-28 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109427,
                name: null,
                label: 'BTC 2018-06-29 Call $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790634,
                name: null,
                label: 'BTC 2018-06-29 Put $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11109428,
                name: null,
                label: 'BTC 2018-06-29 Put $10000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'USD',
                active: true,
                type: 'put',
                strike_price: 1000000,
                min_increment: 25,
                date_live: '2017-11-17 22:30:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract'
            }, {
                id: 11790633,
                name: null,
                label: 'BTC 2018-06-29 Call $15000.00',
                underlying_asset: 'BTC',
                collateral_asset: 'BTC',
                active: true,
                type: 'call',
                strike_price: 1500000,
                min_increment: 25,
                date_live: '2017-12-04 05:00:00+0000',
                date_expires: '2018-06-29 20:00:00+0000',
                derivative_type: 'options_contract' } ],
            meta: {
                limit: 5,
                offset: 15,
                next: '/contracts?offset=20&limit=5',
                previous: '/contracts?offset=10&limit=5',
                total_count: 275
            }
        }
    }
];

module.exports = {
    token: mockToken,
    r: mockR(mockService)
};


let assert = require('assert');
let LXRest = require('./index');
const mock = require('./mock-service');

describe('LXRest', function() {
    describe('#getContracts()', function() {
        it('should work with no limit or offset', function(done) {
            const rest = new LXRest(mock);
            rest.getContracts({}, function(err, result) {
                if (err) {
                    return done(err);
                }
                assert.deepEqual(result.meta, {
                    limit: 40,
                    offset: 0,
                    next: '/contracts?offset=40&limit=40',
                    previous: null,
                    total_count: 275 });
                done();
            });
        });
        it('should work with no limit', function(done) {
            const rest = new LXRest(mock);
            rest.getContracts({ offset: 120 }, function(err, result) {
                if (err) {
                    return done(err);
                }
                assert.deepEqual(result.meta, {
                    limit: 40,
                    offset: 120,
                    next: '/contracts?offset=160&limit=40',
                    previous: '/contracts?offset=80&limit=40',
                    total_count: 275 });
                done();
            });
        });
        it('should work with no offset', function(done) {
            const rest = new LXRest(mock);
            rest.getContracts({ limit: 5 }, function(err, result) {
                if (err) {
                    return done(err);
                }
                assert.deepEqual(result.meta, {
                    limit: 5,
                    offset: 0,
                    next: '/contracts?offset=5&limit=5',
                    previous: null,
                    total_count: 275 });
                done();
            });

        });

        it('shold work with limit and offset', function(done) {
            const rest = new LXRest(mock);
            rest.getContracts({ limit: 5, offset: 15 }, function(err, result) {
                if (err) {
                    return done(err);
                }
                assert.deepEqual(result.meta, {
                    limit: 5,
                    offset: 15,
                    next: '/contracts?offset=20&limit=5',
                    previous: '/contracts?offset=10&limit=5',
                    total_count: 275 });
                done();
            });
        });

    });
    describe('#getOrders()', function() {
        it('should getOrders without error', function(done) {
            const rest = new LXRest(mock);
            rest.getOrders(function(err, result) {
                if (err) {
                    done(err);
                } else {
                    assert.deepEqual(
                        result,
                        { data: { open_orders: [] } });
                    done();
                }
            });
        });
    });
});
